﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para Resumen.xaml
    /// </summary>
    public partial class Resumen : UserControl
    {
        public Resumen()
        {
            InitializeComponent();

            if (MainWindow.a == 1)
            {
                button.Click -=Button_Click;
            }
            try
            {
                Activos.Text = BaseData.versiónOriginal.Socios.Count(s => s.Condición == Condición.Activo).ToString();
                Bloqueados.Text = BaseData.versiónOriginal.Socios.Count(s => s.Condición == Condición.Bloqueado).ToString();
                Bajas.Text = BaseData.versiónOriginal.Socios.Count(s => s.Condición == Condición.DeBaja).ToString();
                Disponibles.Text = (BaseData.versiónOriginal.Películas.Count(p => p.Estado == Estado.Disponible) +
                    BaseData.versiónOriginal.Videojuegos.Count(v => v.Estado == Estado.Disponible)).ToString();
                Deteriorados.Text = (BaseData.versiónOriginal.Películas.Count(p => p.Estado == Estado.Deteriorado) +
                    BaseData.versiónOriginal.Videojuegos.Count(v => v.Estado == Estado.Deteriorado)).ToString();
                Extraviados.Text = (BaseData.versiónOriginal.Películas.Count(p => p.Estado == Estado.Extraviado) +
                    BaseData.versiónOriginal.Videojuegos.Count(v => v.Estado == Estado.Extraviado)).ToString();
                Alquilados.Text = (BaseData.versiónOriginal.Películas.Count(p => p.Estado == Estado.Alquilado) +
                    BaseData.versiónOriginal.Videojuegos.Count(v => v.Estado == Estado.Alquilado)).ToString();
                StreamReader reader;
                reader = File.OpenText("Resumen.txt");
                Alquileres.Text = reader.Read().ToString();
                resumen.Text = reader.ReadToEnd();
                reader.Close();
            }

            catch(Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StreamWriter file;
                file = File.CreateText("Resumen.txt");
                file.WriteLine(Alquileres.Text);
                file.WriteLine(resumen.Text);
                file.Close();
            }

            catch (Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }
    }
}
