﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para Alta.xaml
    /// </summary>
    public partial class Alta : UserControl
    {
        public Alta()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Socio nuevo = new Socio()
                {
                    Email = email.Text,
                    Teléfono = teléfono.Text,
                    NIF = nif.Text,
                    Nombre = nombre.Text,
                    Apellidos = apellidos.Text,
                    Domicilio = domicilio.Text,
                    Teléfono2 = teléfono2.Text,
                    Alta = DateTime.Now,
                    VIP="0"
                };
                if (MainWindow.a == 1)
                {
                    button.Click -= Button_Click;
                    button1.Click -= Button1_Click;
                }
                BaseData.versiónOriginal.Socios.Add(nuevo);
                try
                {
                    int.Parse(teléfono.Text);
                }
                catch
                {
                    MessageBox.Show("Por favor, asegurese que el apartado para 'TELÉFONO' solo tiene números");
                    teléfono.Text = "";
                }
                if (nif.Text == "" || nombre.Text == "" || apellidos.Text == "" || domicilio.Text == "" || teléfono.Text == "" || email.Text == "")
                {
                    MessageBox.Show("Por favor, asegurese de llenar los cuadros de 'NIF','NOMBRE','APELLIDOS','DOMICILIO','TELÉFONO' e 'EMAIL");
                }
                else
                {
                    BaseData.versiónOriginal.SaveChanges();
                    email.Clear();
                    teléfono.Clear();
                    nif.Clear();
                    nombre.Clear();
                    apellidos.Clear();
                    domicilio.Clear();
                    teléfono.Clear();
                    teléfono2.Clear();
                }
            }
            catch(Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Socio baja = BaseData.versiónOriginal.Socios.ToList().First(S => S.NIF == textBox.Text);
                BaseData.versiónOriginal.Socios.Local.Remove(baja);
                BaseData.versiónOriginal.SaveChanges();
                textBox.Clear();
            }
            catch(Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }
    }
}
