﻿
using System.Windows;
using System.Data.SqlClient;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public static string ConnectionString = null;
        public static Rol rol = Rol.Lector;
        public Window1()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int num = 0;
            try
            {
                using(SqlConnection connection=new SqlConnection("DATA SOURCE="+textBox.Text+"INTEGRATED SECURITY=TRUE"))
                {
                    connection.Open();
                }
            }
            catch
            {
                num = 1;
            }
            if (num == 0)
            {
                if (usuario.Text != "berón" && usuario.Text != "jordi")
                {
                    foreach (User user in BaseData.versiónOriginal.Usuarios)
                    {
                        if (user.usuario == usuario.Text)
                        {
                            if (user.contraseña != contraseña.Password.ToString())
                            {
                                MessageBox.Show("Compruebe que el usuario y contraseña son correctos.");
                                num = 1;
                                break;
                            }
                            rol = user.Rol;
                        }
                    }
                }
                else
                {
                    if (contraseña.Password.ToString() == "león")
                    {
                        rol = Rol.Escritor;
                    }
                    else if (contraseña.Password == "cipsa")
                    {
                        rol = Rol.Lector;
                    }
                    else
                    {
                        MessageBox.Show("Compruebe que el usuario y contraseña son correctos.");
                        num = 1;
                    }
                }
                if (num == 0)
                {
                    MainWindow.cadena = "DATA SOURCE=" + textBox.Text + ";initial catalog=VersiónOriginal;integrated security=true;connection timeout=5";
                    ConnectionString = "adfadf";
                    Close();
                }
                textBox.Text = "";
                contraseña.Password = "";
            }
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
