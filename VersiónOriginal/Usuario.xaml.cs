﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para Usuario.xaml
    /// </summary>
    public partial class Usuario : UserControl
    {
        public Usuario()
        {
            InitializeComponent();
            if (MainWindow.a == 1)
            {
                button.Click -=Button_Click;
                button_Copy1.Click -=Button_Copy1_Click;
            }
        }

        private void Button_Copy1_Click(object sender, RoutedEventArgs e)
        {
            User newUser = new User
            {
                usuario = user.Text,
                contraseña = password.Text,
                Rol=Rol.Lector
            };
            user.Text = "";
            password.Text = "";
            BaseData.versiónOriginal.Usuarios.Add(newUser);
            try
            {
                BaseData.versiónOriginal.SaveChanges();
            }
            catch(Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            User newUser = new User
            {
                usuario = user.Text,
                contraseña = password.Text,
                Rol = Rol.Escritor
            };
            user.Text = "";
            password.Text = "";
            BaseData.versiónOriginal.Usuarios.Add(newUser);
            try
            {
                BaseData.versiónOriginal.SaveChanges();
            }
            catch (Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }
    }
}
