﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para Agregar.xaml
    /// </summary>
    public partial class Agregar : UserControl
    {
        public Agregar()
        {
            InitializeComponent();
            try
            {
                dataGrid.ItemsSource = BaseData.versiónOriginal.Películas.Local;
                dataGrid1.ItemsSource = BaseData.versiónOriginal.Videojuegos.Local;
                dataGrid.AutoGenerateColumns = false;
                dataGrid.Columns.Add(Columna("Código"));
                dataGrid.Columns.Add(Columna("Título"));
                dataGrid.Columns.Add(Combo("Estado"));
                dataGrid.Columns.Add(Check("Edad18"));
                dataGrid.Columns.Add(Columna("Duración"));
                dataGrid.Columns.Add(Columna("DVDs"));
                dataGrid.Columns.Add(Columna("Adquisición"));
                dataGrid.Columns.Add(Columna("Producción"));
                dataGrid1.AutoGenerateColumns = false;
                dataGrid1.Columns.Add(Columna("Código"));
                dataGrid1.Columns.Add(Columna("Título"));
                dataGrid1.Columns.Add(Combo("Estado"));
                dataGrid1.Columns.Add(new DataGridComboBoxColumn { Header = "Plataforma", SelectedItemBinding = new Binding("Plataforma"), ItemsSource = Enum.GetNames(typeof(Plataforma)) });
                dataGrid1.Columns.Add(Columna("Discos"));
            }
            catch(Exception i)
            {
                MessageBox.Show(i.Message+"; "+i.InnerException);
            }
            if (MainWindow.a == 1)
            {
                button.Click -= Button_Click;
            }
            void but(object sender, RoutedEventArgs e) { }
        }
        public DataGridTextColumn Columna(string título)
        {
            return new DataGridTextColumn { Header = título, Binding = new Binding(título) };
        }

        public DataGridComboBoxColumn Combo(string título)
        {
            return new DataGridComboBoxColumn { Header = título, SelectedItemBinding = new Binding(título), ItemsSource = Enum.GetNames(typeof(Estado))};
        }

        public DataGridCheckBoxColumn Check(string título)
        {
            return new DataGridCheckBoxColumn { Header = título, Binding = new Binding(título) };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BaseData.versiónOriginal.SaveChanges();
        }
    }
}
