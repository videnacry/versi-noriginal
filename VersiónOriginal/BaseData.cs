﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace VersiónOriginal
{
    public enum Estado { Disponible,Alquilado,Deteriorado,Extraviado}
    public enum Plataforma { xbox,ps2,ps3,wii,pc}
    public enum Condición { Activo,Bloqueado,DeBaja}
    public enum Rol {Lector,Escritor}



    public class BaseData:DbContext
    {
        public BaseData() : base(MainWindow.cadena)
        {

        }
        public static BaseData versiónOriginal = null;        
        public static void Wesly()
        {
            versiónOriginal = new BaseData();
        }
        public DbSet<Película> Películas { get; set; }
        public DbSet<Videojuego> Videojuegos { get; set; }
        public DbSet<Socio> Socios { get; set; }        
        public DbSet<User> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Socio>().HasMany(s => s.Videojuego).WithOptional(v => v.Socio).HasForeignKey(v => v.Ocupante);
            modelBuilder.Entity<Socio>().HasMany(s => s.Película).WithOptional(p => p.Socio).HasForeignKey(p => p.Ocupante);
        }
    }

    public class Película
    {
        [Key, StringLength(10)]
        public string Código { get; set; }
        [StringLength(30)]
        public string Título { get; set; }
        public Estado Estado { get; set; }
        public bool Edad18 { get; set; }
        public int Duración { get; set; }
        public int DVDs { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? Adquisición { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? Producción { get; set; }
        [StringLength(10)]
        public string Ocupante { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? Devolución{get;set;}

        public Socio Socio { get; set; }
    }



    public class Videojuego
    {

        [Key]
        [StringLength(10)]
        public string Código { get; set; }
        [StringLength(30)]
        public string Título { get; set; }
        public Estado Estado { get; set; }
        public int Discos { get; set; }
        public Plataforma Plataforma { get; set; }
        [StringLength(10)]
        public string Ocupante { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? Devolución { get; set; }

        public Socio Socio { get; set; }
    }


    public class Socio
    {
        public Socio()
        {
            Película = new HashSet<Película>();
            Videojuego = new HashSet<Videojuego>();
        }

        [Key,StringLength(10)]
        public string NIF { get; set; }
        [StringLength(10)]
        public string Nombre { get; set; }
        [StringLength(20)]
        public string Apellidos { get; set; }
        public Condición Condición { get; set; }
        public string Email { get; set; }
        [StringLength(20)]
        public string Domicilio { get; set; }
        [StringLength(10)]
        public string Teléfono { get; set; }
        [StringLength(10)]
        public string Teléfono2 { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? Alta { get; set; }
        [StringLength(3)]
        public string VIP { get; set; }

        public HashSet<Película> Película { get; set; }
        public HashSet<Videojuego> Videojuego { get; set; }
    }

    public class User
    {
        [Key,StringLength(20)]
        public string usuario { get; set; }
        [StringLength(20)]
        public string contraseña { get; set; }
        public Rol Rol { get; set; }
    }
}
