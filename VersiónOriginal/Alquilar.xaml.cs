﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.IO;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para Alquilar.xaml
    /// </summary>
    public partial class Alquilar : UserControl
    {
        static public int num = 0;
        public Alquilar()
        {
            InitializeComponent();

            if (MainWindow.a == 1)
            {
                button.Click -= Button_Click;
                button_Copy.Click -=Button_Click1;
            }
            try
            {
                StreamReader reader;
                reader = File.OpenText("Resumen.txt");
                num = (reader.Read() > 0) ? reader.Read() : 0;
                reader.Close();
            }
            catch(Exception i)
            {
                MessageBox.Show(i.Message);
                StreamWriter writer;
                writer = File.CreateText("Resumen.txt");
                writer.Close();
            }
            var NIF = (from socio in BaseData.versiónOriginal.Socios.ToList() where socio.Condición == Condición.Activo select socio.NIF).ToList();
            comboBox.ItemsSource = NIF;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int nim = 0,nam=0;  
                try
                {
                    if (Código.Text != null)
                    {
                        foreach (Película peli in BaseData.versiónOriginal.Películas)
                        {
                            if (peli.Código == Código.Text)
                            {
                                if (peli.Estado != Estado.Disponible)
                                {
                                    MessageBox.Show("El producto no se encuentra disponible");
                                    break;
                                }
                                peli.Ocupante = NIF.Text;
                            peli.Devolución = DateTime.Today + new TimeSpan(int.Parse(Días.Text), 0, 0, 0);
                                peli.Estado = Estado.Alquilado;
                                BaseData bas = new BaseData();
                                nim = 1;
                                break;
                            }
                        }

                        foreach (Videojuego juego in BaseData.versiónOriginal.Videojuegos)
                        {
                            if (juego.Código == Código.Text)
                            {
                                if (juego.Estado != Estado.Disponible)
                                {
                                    MessageBox.Show("El producto no se encuentra disponible");
                                    break;
                                }
                                juego.Ocupante = NIF.Text;
                                juego.Devolución = DateTime.Today + new TimeSpan(int.Parse(Días.Text), 0, 0,0);
                                juego.Estado = Estado.Alquilado;
                                nim = 1;
                                break;
                            }
                        }
                    }
                else 
                {
                        foreach (Película peli in BaseData.versiónOriginal.Películas)
                        {
                            if (peli.Título == Título.Text)
                            {
                                if (peli.Estado != Estado.Disponible)
                                {
                                    MessageBox.Show("El producto no se encuentra disponible");
                                    break;
                                }
                                peli.Ocupante = NIF.Text;
                                peli.Devolución = DateTime.Today + new TimeSpan(int.Parse(Días.Text), 0, 0);
                                peli.Estado = Estado.Alquilado;
                                nim = 1;
                                break;
                            }
                        }


                        foreach (Videojuego peli in BaseData.versiónOriginal.Videojuegos)
                        {
                            if (peli.Título == Título.Text)
                            {
                                if (peli.Estado != Estado.Disponible)
                                {
                                    MessageBox.Show("El producto no se encuentra disponible");
                                    break;
                                }
                                peli.Ocupante = NIF.Text;
                                peli.Devolución = DateTime.Today + new TimeSpan(int.Parse(Días.Text), 0, 0);
                                peli.Estado = Estado.Alquilado;
                                nim = 1;
                                break;
                            }
                        }
                    }
                if (nim == 0)
                    MessageBox.Show("Verifique que el código es correcto (mayúsculas, minúsculas..)");
                if (nim > 0)
                {
                    foreach(Socio soci in BaseData.versiónOriginal.Socios)
                    {
                        if (soci.NIF == NIF.Text)
                        {
                            if (soci.Condición != Condición.Activo)
                                MessageBox.Show("El socio no se encuentra disponible");
                            if (int.Parse(soci.VIP) > 30)
                            {
                                nam = 2;
                                MessageBox.Show("-50%");
                            }
                            if (int.Parse(soci.VIP) > 20&&nam!=2)
                            {
                                nam = 2;
                                MessageBox.Show("-25%");
                            }
                            if (int.Parse(soci.VIP) > 15 && nam != 2)
                            {
                                nam = 2;
                                MessageBox.Show("-15%");
                            }
                            if (int.Parse(soci.VIP) > 10 && nam != 2)
                            {
                                nam = 2;
                                MessageBox.Show("-10%");
                            }
                            if (int.Parse(soci.VIP) > 5 && nam != 2)
                            {
                                MessageBox.Show("-10%");
                            }
                            soci.VIP = (int.Parse(soci.VIP) + 1).ToString();
                            nam = 1;
                        }
                    }
                    if (nam == 0)
                        MessageBox.Show("El socio no existe");
                }
                if(nam>0)
                    num+=1;
                try
                {
                    BaseData.versiónOriginal.SaveChanges();
                }
                catch (Exception i)
                {
                    MessageBox.Show(i.Message);
                }
            }
                catch(Exception i)
                { MessageBox.Show(i.Message +"; "+i.InnerException); }        
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            StreamWriter writer;
            writer = File.AppendText("Resumen.txt");
            writer.Write(num.ToString());
            writer.Close();            
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NIF.Text = comboBox.SelectedItem.ToString();
        }
    }
}
