﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Data.SqlClient;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Rejilla rejilla = new Rejilla();
        public static Doble doble = new Doble();
        public static int a = 0;
        public static string cadena = null;
        public MainWindow()
        {
            int num = 1;
            InitializeComponent();
            new Window1().ShowDialog();
            try
            {
                using (SqlConnection connection = new SqlConnection(cadena))
                {
                    connection.Open();
                    num = 0;
                }
            }
            catch
            {
                Close();
            }
            if (num == 0)
            {
                BaseData.Wesly();
                if (Window1.ConnectionString == null)
                    Close();
                switch (Window1.rol)
                {
                    case Rol.Lector:
                        a = 1;
                        break;
                    case Rol.Escritor:
                        a = 2;
                        break;
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new Alquilar());
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new Devolución());
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new Agregar());
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new Alta());
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            try
            {
                grid.Children.Clear();
                List<Socio> sociosActivos = (from socio in BaseData.versiónOriginal.Socios.ToList() where socio.Condición == Condición.Activo select socio).ToList();
                rejilla.dataGrid.ItemsSource = sociosActivos;
                grid.Children.Add(rejilla);
            }

            catch (Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            try
            {
                grid.Children.Clear();
                List<Socio> sociosInactivos = (from socio in BaseData.versiónOriginal.Socios.ToList() where socio.Condición == Condición.Bloqueado select socio).ToList();
                rejilla.dataGrid.ItemsSource = sociosInactivos;
                grid.Children.Add(rejilla);
            }

            catch (Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            try
            {
                grid.Children.Clear();
                var pelis = (from peli in BaseData.versiónOriginal.Películas.ToList() where peli.Estado == Estado.Disponible select peli).ToList();
                doble.dataGrid.ItemsSource = pelis;
                var juegos = (from juego in BaseData.versiónOriginal.Videojuegos.ToList() where juego.Estado == Estado.Disponible select juego).ToList();
                doble.dataGrid2.ItemsSource = juegos;
                grid.Children.Add(doble);
            }

            catch (Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void MenuItem_Click_7(object sender, RoutedEventArgs e)
        {
            try
            {
                grid.Children.Clear();
                var pelis = (from peli in BaseData.versiónOriginal.Películas.ToList() where peli.Estado == Estado.Alquilado select peli).ToList();
                doble.dataGrid.ItemsSource = pelis;
                var juegos = (from juego in BaseData.versiónOriginal.Videojuegos.ToList() where juego.Estado == Estado.Alquilado select juego).ToList();
                doble.dataGrid2.ItemsSource = juegos;
                grid.Children.Add(doble);
            }

            catch (Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void MenuItem_Click_8(object sender, RoutedEventArgs e)
        {
            try
            {
                grid.Children.Clear();
                var pelis = (from peli in BaseData.versiónOriginal.Películas.ToList() where peli.Estado == Estado.Deteriorado select peli).ToList();
                doble.dataGrid.ItemsSource = pelis;
                var juegos = (from juego in BaseData.versiónOriginal.Videojuegos.ToList() where juego.Estado == Estado.Deteriorado select juego).ToList();
                doble.dataGrid2.ItemsSource = juegos;
                grid.Children.Add(doble);
            }

            catch (Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void MenuItem_Click_9(object sender, RoutedEventArgs e)
        {
            try
            {
                grid.Children.Clear();
                var pelis = (from peli in BaseData.versiónOriginal.Películas.ToList() where peli.Estado == Estado.Extraviado select peli).ToList();
                doble.dataGrid.ItemsSource = pelis;
                var juegos = (from juego in BaseData.versiónOriginal.Videojuegos.ToList() where juego.Estado == Estado.Extraviado select juego).ToList();
                doble.dataGrid2.ItemsSource = juegos;
                grid.Children.Add(doble);
            }
            catch(Exception i)
            {
                MessageBox.Show(i.Message);
            }
        }

        private void MenuItem_Click_10(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new Resumen());
        }

        private void MenuItem_Click_11(object sender, RoutedEventArgs e)
        {
            grid.Children.Clear();
            grid.Children.Add(new Usuario());
        }

        private void Cambiar_Click(object sender, RoutedEventArgs e)
        {   
            new Window1().ShowDialog();
            BaseData.Wesly();
            if (Window1.ConnectionString == null)
                Close();
            switch (Window1.rol)
            {
                case Rol.Lector:
                    a = 1;
                    break;
                case Rol.Escritor:
                    a = 2;
                    break;
            }
        }
    }
}
