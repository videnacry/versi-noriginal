﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace VersiónOriginal
{
    /// <summary>
    /// Lógica de interacción para Devolución.xaml
    /// </summary>
    public partial class Devolución : UserControl
    {
        public Devolución()
        {
            InitializeComponent();

            if (MainWindow.a == 1)
            {
                button.Click -=Button_Click;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Extra.Text = "-";
                foreach (Película peli in BaseData.versiónOriginal.Películas)
                {
                    if (peli.Código == Código.Text)
                    {
                        Extra.Text = peli.Devolución.ToString() + " - " + DateTime.Today.ToString();
                        peli.Devolución = null;
                        peli.Ocupante = null;
                        peli.Estado = Estado.Disponible;
                    }
                }
                if (textBlock.Text == "-")
                    foreach (Videojuego juego in BaseData.versiónOriginal.Videojuegos)
                    {
                        Extra.Text = juego.Devolución.ToString() + " - " + DateTime.Today.ToString();
                        juego.Devolución = null;
                        juego.Ocupante = null;
                        juego.Estado = Estado.Disponible;
                    }
                BaseData.versiónOriginal.SaveChanges();
                Código.Clear();
            }
            catch(Exception i)
            { MessageBox.Show(i.Message+" "+i.InnerException); }
        }
    }
}
